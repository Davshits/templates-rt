import pytest

from tests.testProcess import read_line, write_line, get_process, read_lines

program_under_test = './../cmake-build-debug/lab3_2'
program_args = []
second = 1
action_timeout = 0.5 * second


async def write_lines(input_str: str) -> [str]:
    process = await get_process(program_under_test, *program_args)
    write_line(process.stdin, input_str.encode("utf-8"))
    byte_lines = await read_lines(process.stdout, action_timeout)
    decoded_lines = [x.decode("utf-8") for x in byte_lines]
    result = "".join(decoded_lines)
    return result


@pytest.mark.asyncio
async def test_1():
    expected_output = "Введите значение a:\nРезультат:\n147455.0000000\n"
    input_str = "1\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_2():
    expected_output = "Введите значение a:\nРезультат:\n147456.0000000\n"
    input_str = "0\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_2():
    expected_output = "Введите значение a:\nРезультат:\n147455.5000000\n"
    input_str = "0.5\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_3():
    expected_output = "Введите значение a:\nРезультат:\n28.0000000\n"
    input_str = "-6\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_4():
    expected_output = "Введите значение a:\nРезультат:\n147444.0000000\n"
    input_str = "12\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_5():
    expected_output = "Введите значение a:\nРезультат:\n28.0000000\n"
    input_str = "-1\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output
