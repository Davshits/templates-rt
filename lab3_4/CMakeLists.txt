cmake_minimum_required(VERSION 3.23)
project(lab3_4 C)

set(CMAKE_C_STANDARD 17)

add_executable(lab3_4 main.c)
IF (NOT WIN32)
target_link_libraries(lab3_4 m)
ENDIF()
