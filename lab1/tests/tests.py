import pytest

from tests.testProcess import read_line, write_line, get_process

program_under_test = './../cmake-build-debug/lab1'
program_args = []
second = 1
action_timeout = 0.5 * second


async def get_message() -> str:
    process = await get_process(program_under_test, *program_args)
    line = await read_line(process.stdout, action_timeout)
    received = line.decode("utf-8")
    return received


@pytest.mark.asyncio
async def test_message():
    expected = 'Привет, бауманец!'
    received = await get_message()
    assert received == expected
