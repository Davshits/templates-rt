import asyncio.subprocess


async def read_line(stream: asyncio.StreamReader, timeout: float):
    try:
        return await asyncio.wait_for(stream.readline(), timeout=timeout)
    except asyncio.TimeoutError:
        return ""


def write_line(stream: asyncio.StreamWriter, data: bytes) -> bytes:
    try:
        stream.write(data)
    except asyncio.TimeoutError:
        return b''


async def get_process(program_under_test: str, *args) -> asyncio.subprocess.Process:
    return await asyncio.create_subprocess_exec(program_under_test, *args, stdout=asyncio.subprocess.PIPE,
                                                stdin=asyncio.subprocess.PIPE)
